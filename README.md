# Level Up as a Data Analyst

## Resources

### Books

- [R for Data Science (R4DS)](https://r4ds.had.co.nz) (textbook)
- [R4DS Exercise Solutions](https://jrnold.github.io/r4ds-exercise-solutions/)
- [Principles and Techniques of Data Science](https://www.textbook.ds100.org/) (highly recommended)
- [Text Mining with R](https://www.tidytextmining.com/)
- [Tidyverse Cookbook](https://bookdown.org/Tazinho/Tidyverse-Cookbook/)
- [ModernDive - A moderndive into R and the tidyverse](https://moderndive.com/index.html)
- [Building Big Shiny Apps - A Workflow](https://thinkr-open.github.io/building-shiny-apps-workflow/)
- [Advanced R](http://adv-r.had.co.nz/)

### Articles

- [Why do dogs ...?](https://whydocatsanddogs.com/dogs)
- [Regressing modeling](https://tidymodels.github.io/parsnip/articles/articles/Regression.html)
- [Classification modeling](https://tidymodels.github.io/parsnip/articles/articles/Classification.html)
- [Project-oriented workflow](https://www.tidyverse.org/articles/2017/12/workflow-vs-script/)
- [Exploring bike rental behavior (Part 1)](http://www.storybench.org/exploring-bike-rental-behavior-using-r/)
- [Modeling bike rental with gradient boosting machine (Part 2)](http://www.storybench.org/tidytuesday-bike-rentals-part-2-modeling-with-gradient-boosting-machine/)

### Misc

- [RStudio Cheat Sheets](https://www.rstudio.com/resources/cheatsheets/)
- [R Weekly: Weekly Updates from the Entire R Community](https://rweekly.org/)
- [Data Science Weekly Newsletter](https://www.datascienceweekly.org/)
- [TidyTuesday: A weekly data project in R from the R4DS online learning community](https://github.com/rfordatascience/tidytuesday)
- [R4DS Online Learning Community](https://www.rfordatasci.com/)

## Datasets

- [Tweets Julia](https://github.com/dgrtwo/tidy-text-mining/blob/master/data/tweets_julia.csv)
- [Tweets Dave](https://github.com/dgrtwo/tidy-text-mining/blob/master/data/tweets_dave.csv)

## Data Lab

### Level 1

- [Setup](data-lab/001.md#setup-workflow)
- [Code Style](data-lab/001.md#code-style-workflow)
- [Documentation](data-lab/001.md#documentation-workflow)
- [Data basics](data-lab/001.md#data-basics)
- [Visualization basics](data-lab/001.md#visualization-basics-1-explore)
- [Manipulation basics](data-lab/001.md#manipulation-basics-wrangle)
- [R Markdown basics](data-lab/001.md#r-markdown-basics-communicate)

### Level 2

- [Visualization basics 2](data-lab/002.md#visualisation-basics-2-explore)
- [Labels](data-lab/002.md#labels-communicate)
- [Data structure basics](data-lab/002.md#data-structure-basics-program)
- [Exploratory data analysis (1D)](data-lab/002.md#exploratory-data-analysis-1d-explore)
- [Tidy data](data-lab/002.md#tidy-data-wrangle)
- [ggplot2 calls](data-lab/002.md#ggplot2-calls-visualize)

### Level 3

- [Missing values](data-lab/003.md#missing-values-wrangle)
- [Essentials of relational data](data-lab/003.md#essentials-of-relational-data-wrangle)
- [Scales](data-lab/003.md#scales-visualize)
- [Importing data](data-lab/003.md#importing-data-wrangle)
- [Spreading and gathering](data-lab/003.md#spreading-and-gathering-wrangle)
- [Function basics](data-lab/003.md#function-basics-program)

### Level 4

- [Anonymous functions](data-lab/004.md#anonymous-functions-program)
- [purrr basics](data-lab/004.md#purrr-basics-program)
- [Exploratory Data Analysis (2D)](data-lab/004.md#exploratory-data-analysis-2d-explore)
- [String basics](data-lab/004.md#string-basics-wrangle)
- [Perception](data-lab/004.md#perception-explore)
- [RMarkdown](data-lab/004.md#rmarkdown-workflow)
- [Window functions](data-lab/004.md#window-functions-wrangle)
