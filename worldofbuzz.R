library(rvest)
library(tidyverse)

duration_to_date <- function (duration) {
  library(lubridate)
  n <- parse_integer(str_extract(duration, "[0-9]+"))
  units = str_trim(str_extract(duration, " [dw]"))
  if_else(units == "d", ymd(today() - ddays(n)), ymd(today() - dweeks(n)))
}

fetch_news <- function (url) {
  Sys.sleep(0.4)
  temp <- read_html(url)
  temp %>%
    html_nodes("div#mvp-content-body p") %>%
    html_text() %>%
    str_c(collapse = " ")
}

wow <- read_html("https://www.worldofbuzz.com/viral/wow/")

headlines <- wow %>%
  html_nodes("li.mvp-blog-story-wrap h2") %>%
  html_text()

urls <- wow %>%
  html_nodes("li.mvp-blog-story-wrap a") %>%
  html_attr("href")

durations <- wow %>%
  html_nodes("li.mvp-blog-story-wrap span.mvp-cd-date") %>%
  html_text()

df <- tibble(
  headline = headlines,
  url = urls,
  duration = durations
)

df %>%
  mutate(date = duration_to_date(duration)) %>%
  group_by(date) %>%
  count() %>%
  ggplot() +
  geom_line(aes(x = date, y = n))

f <- function (urls) {
  map_chr(urls, fetch_news)
}

df <- df %>%
  mutate(
    contents = f(url)
  )

df <- df %>%
  mutate(
    date = duration_to_date(duration),
    contents = map_chr(url, fetch_news)
  )
