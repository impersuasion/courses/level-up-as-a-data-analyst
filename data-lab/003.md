# Data Lab: Level 3

## Missing values `[wrangle]`

R uses `NA` to present unknown, but potentially measurable variables.

The pattern of missingness in a dataset is often informative, so you should never silently ignore
missing values.

- [Missing values (transformation)](http://r4ds.had.co.nz/transform.html#missing-values)
  [r4ds-5.2.3]

- [Missing values (summary)](http://r4ds.had.co.nz/transform.html#missing-values-1)
  [r4ds-5.6.2]

- [Missing values (visualisation)](http://r4ds.had.co.nz/exploratory-data-analysis.html#missing-values-2)
  [r4ds-7.4]

## Essentials of relational data `[wrangle]`

It is extremely rare to only require a single table of data for an analysis. Far more often you'll
need to combine together multiple sources of information. Interconnected datasets are often called
**relational** because you need to care about the relationships between the datasets.

- [Introduction](http://r4ds.had.co.nz/relational-data.html#introduction-7)
  [r4ds-13.1]

- [nycflights13](http://r4ds.had.co.nz/relational-data.html#nycflights13-relational)
  [r4ds-13.2]

- [Keys](http://r4ds.had.co.nz/relational-data.html#keys)
  [r4ds-13.3]

- [Mutating joins](http://r4ds.had.co.nz/relational-data.html#mutating-joins)
  [r4ds-13.4]

- [Filtering joins](https://r4ds.had.co.nz/relational-data.html#filtering-joins)
  [r4ds-13.5]

## Scales `[visualize]`

ggplot2 scales take variables (numeric, character, ...) and convert them to visual properties
(colour, shape, ...).

- [Scales](http://r4ds.had.co.nz/graphics-for-communication.html#scales)
  [r4ds-28.4]

## Importing data `[wrangle]`

So far you've worked with datasets that come with R packages. Now it's time to
learn how to read simple flat files from disk. We'll be using functions from
`readr`, a core `tidyverse` package.

### Delimited files

Delimited files have a **delimiter** between each value. Two types make up the
majority of delimited files that you'll see in the wild:

- csv (comma separated)
- tsv (tab separated)

We'll focus on csv files, but everything you'll learn applies equally to tsvs,
replacing commas with tabs.

A typical csv file looks something like this:

```
Sepal.Length,Sepal.Width,Petal.Length,Petal.Width,Species
5.1,3.5,1.4,0.2,setosa
4.9,3,1.4,0.2,setosa
4.7,3.2,1.3,0.2,setosa
4.6,3.1,1.5,0.2,setosa
5,3.6,1.4,0.2,setosa
5.4,3.9,1.7,0.4,setosa
4.6,3.4,1.4,0.3,setosa
5,3.4,1.5,0.2,setosa
```

Note that:

- The first line gives the column names
- Each subsequent line is one row of data
- Each value is separated by a comma (hence the name)

Typically a csv file has the `.csv` extension but the extension can really be
anything. If you are getting weird errors when reading a file, it's a good idea
to peek inside the file using `readr::read_lines()` and `writeLines()`.

```
"myfile.csv" %>% 
  read_lines(n_max = 10) %>%
  writeLines()
```

The workhorse for reading in csv files is called `read_csv()`. You give it a
path to a csv file and it gives you back a tibble:

```
heights <- read_csv("heights.csv")
heights
```

If you are very lucky, you can point `read_csv()` at a file and it just works.
But this is usually the exception, not the rule, and often you'll need to tweak
some arguments.

The most important arguments to `read_csv()` are:

- `col_names`: usually `col_names = TRUE` which tells `read_csv()` that the
  first line of the file is the column names. If there aren't any column names
  set `col_names = FALSE` or supply a character vector telling `read_csv()`
  what they should be `col_names = c("x", "y", "z")`.

- `col_types`: you might have noticed that when we called `read_csv()` above it
  printed out a list of column "specifications". That describes how `readr`
  converts each column into a data structure. `readr` uses some pretty good
  heuristics to guess the type, but sometimes the heuristics fail and you'll
  need to supply the truth.

- It's fairly common to encounter csv files that have a bunch of junk at the top. You can use
  `skip = n` to skip the first `n` lines, or `comment = "#"` to ignore all lines that start with
  `#`.

- `read_csv()` expects missing values to be suppled as `NA`. If your file uses a different
  convention, use `na = "."` to override the default.

### Handling more complicated situations

You have the basics of parsing files, but now it's time to learn how to handle
more complicated situations where readr doesn't automatically guess the correct
column types:

- How to parse character vectors that are already in R, and how to tell readr
  how to parse vectors from disk.

- How to recognize and fix problems caused by readr failing to guess the
  correct variable type.

#### Readings

- [Parsing a vector](http://r4ds.had.co.nz/data-import.html#parsing-a-vector)
  [r4ds-11.3]

- [Parsing a file](http://r4ds.had.co.nz/data-import.html#parsing-a-file)
  [r4ds-11.4]

- [Data import cheat sheet](https://github.com/rstudio/cheatsheets/blob/master/data-import.pdf)

## Spreading and gathering `[wrangle]`

The two most common ways for data to be messy are to have:

1. One variable spread across multiple columns.
1. One observation scattered across multiple rows.

To fix these problems you need `spread()` and `gather()` from the `tidyr` package.

`spread()` and `gather()` also illustrate a new type of missingness. So far we've discussed
explicit missing values (`NA`), but it's also possible for missing values to be simply absent from
the data.

- [Spreading and gathering](http://r4ds.had.co.nz/tidy-data.html#spreading-and-gathering)
  [r4ds-12.3]

- [Missing values](http://r4ds.had.co.nz/tidy-data.html#missing-values-3)
  [r4ds-12.5]

## Function basics `[program]`

Functions are a powerful tool for reducing the amount of duplication in your code. Reducing
duplication is a good principle because it means that each "fact" in your code is only stored in
one place. When the requirements of your code change (as they invariably do), you only need to
make the change in a single place, reducing the chances of inconsistencies within your code.

- [Introduction](http://r4ds.had.co.nz/functions.html#introduction-12)
  [r4ds-19.1]

- [When should you write a function?](http://r4ds.had.co.nz/functions.html#when-should-you-write-a-function)
  [r4ds-19.2]

- [Functions are for humans and computers](http://r4ds.had.co.nz/functions.html#functions-are-for-humans-and-computers)
  [r4ds-19.3]