```r
installed <- rownames(installed.packages())
packages <- c(
  'janeaustenr',
  'tidytext',
  'rvest',
  'gutenbergr'
)
missing <- packages[!(packages %in% installed)]
if (length(missing) != 0) {
  writeLines("Installing", missing)
  install.packages(missing,
                   repos = "https://cran.rstudio.com")
}
```