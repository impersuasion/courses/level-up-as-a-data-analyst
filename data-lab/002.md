# Data Lab: Level 2

## Visualisation basics 2 `[explore]`

Now it’s time to learn some more of the components that make up ggplot2.

- **Statistical transformations**, or `stats` for short, allow you to plot summaries of the data
  and are needed for important visualisations like the histogram.

- **Position adjustments** help handle overlapping objects. They're essential for bar graphs, but
  are also useful for point data.

- **Coordinate systems** control how the `x` and `y` aesthetics map to position on the plot.
  You'll use them least of any ggplot2 component, but when you do need them they're
  invaluable.

- You'll also learn a bit about the underlying theory of `ggplot2`, the **layered grammar of
  graphics**.

Finally, you'll learn a more compact style of `ggplot2` calls that you'll use throughout the rest
of the course.

- [Statistical transformations](http://r4ds.had.co.nz/data-visualisation.html#statistical-transformations)
  [r4ds-3.7]

- [Position adjustments](http://r4ds.had.co.nz/data-visualisation.html#position-adjustments)
  [r4ds-3.8]

- [Coordinate systems](http://r4ds.had.co.nz/data-visualisation.html#coordinate-systems)
  [r4ds-3.9]

- [The layered grammar of graphics](http://r4ds.had.co.nz/data-visualisation.html#the-layered-grammar-of-graphics)
  [r4ds-3.10]

- [R Graphics Cookbook](http://www.cookbook-r.com/Graphs/)

## Labels `[communicate]`

Good labels make visualisations much easier to understand for newcomers to the data.

- [Label](http://r4ds.had.co.nz/graphics-for-communication.html#label)
  [r4ds-28.2]

## Data structure basics `[program]`

It's helpful to know a little bit about how data structures are organised in R. The simplest data
structure in R is the vector, which can be broken up in to atomic vectors and augmented vectors.
Vectors of related data are organised together in `tibbles` and `data.frames`.

```
library(tidyverse)
```

### Atomic vectors

The atomic vectors are the "atoms" of R, the simple building blocks upon which all else is built.
There are four types of atomic vector that are important for data analysis:

- logical vectors `<lgl>` contain `TRUE` or `FALSE`.
- integer vectors `<int>` contain integers.
- double vectors `<dbl>` contain real numbers.
- character vector `<chr>` contain strings made with `""`.

All vectors can also contain the missing value `NA`. You’ll learn more about missing values later
on. Collectively integer and double vectors are known as *numeric* vectors. The difference is
rarely important in R.

You create atomic vectors by hand with the `c()` function:

```
logical <- c(TRUE, FALSE, FALSE)

# The difference between the real number 1 and the integer 1 is not 
# usually important, but it sometimes comes up. R uses the suffix 
# "L" to indicate that a number is an integer.
integer <- c(1L, 2L, 3L)

double <- c(1.5, 2.8, pi)

character <- c("this", "is", "a character", "vector")
```

### Augmented

Augmented vectors are atomic vectors with additional metadata. There are four important augmented
vectors:

- **factors** `<fct>`, which are used to represent categorical variables can take one of a fixed
  and known set of possible values (called the levels).

- **ordered factors** `<ord>`, which are like factors but where the levels have an intrinsic
  ordering (i.e. it's reasonable to say that one level is "less than" or "greater than" another
  variable).

- **dates** `<dt>`, record a date.

- **date-times** `<dttm>`, which are also known as `POSIXct`, record a date and a time.

For now, you just need to recognise these when you encounter them. You’ll learn how to create each
type of augmented vector later in the course.

### Data frames/tibbles

Related vectors (both atomic and augment) are collected together into data frames or tibbles.
Later you'll learn the precise different between `data.frames` and `tibbles`, but don't worry
about it for now. There are two ways to create tibbles by hand:

1. From individual vectors, each representing a column:

   ```
   my_tibble <- tibble(
     x = c(1, 9, 5),
     y = c(TRUE, FALSE, FALSE),
     z = c("apple", "pear", "banana")
   )
   my_tibble
   ```

2. From individual values, organised in rows:

   ```
   my_tibble <- tribble(
     ~x, ~y,    ~z,
     1,  TRUE,  "apple",
     9,  FALSE, "pear",
     5,  FALSE, "banana"
   )
   my_tibble
   ```

Typically it will be obvious whether you need to use `tibble()` or `tribble()`. One representation
will either be much shorter or much clearer than the other.

### Dimensions

When you print a tibble it tell you its column names and the overall dimensions:

```
diamonds
```

If you want to get access dimensions directly, you have three options:

```
dim(diamonds)

nrow(diamonds)

ncol(diamonds)
```

To get the variable names, use `names()`:

```
names(diamonds)
```

There isn't currently a convenient way to get the variable types, but you can use
`purrr::map_chr()` to apply `type_sum()` (short for type summary) to each variable.

```
type_sum(diamonds)

map_chr(diamonds, type_sum)
```

### Variables

You can extract a variable out of a tibble by using `[[` or `$`:

```r
mtcars[["mpg"]]

mtcars$mpg
```

The dplyr equivalent, which can also be used in a pipe, is `pull()`:

```r
mtcars %>% pull(mpg)
```

## Exploratory data analysis (1D) `[explore]`

Exploratory data analysis (EDA) is partly a set of techniques, but is mostly a mindset: you want
to remain open to what the data is telling you.

```
library(tidyverse)
library(nycflights13)
```

Whenever you start working with a new variable, it's a really good idea to first take a look at
the variable by itself, before you start combining it with other variables. As well as the visual
techniques you'll learn in the readings, another quick and dirty function is `count()`.

`df %>% count(grp)` is shorthand for `df %>% group_by(grp) %>% summarise(n = n())`.

```
flights %>% count(carrier)
```

It has two convenient arguments:

- `sort = TRUE` automatically arranges the result so the most common values are at the top.

  ```
  flights %>% count(dest, sort = TRUE)
  ```

- `wt = my_variable` switches from a count to a weight sum of `my_variable`. For example, the
  following code gives the total amount distance traveled by each carrier. It is particularly
  useful if you have data that has already been aggregated.
  
  ```
  flights %>% count(carrier, wt = distance)
  ```

You can also `count()` the value of expression. This is a useful technique to get a quick count of
how many missing values there are:

```
flights %>% count(is.na(dep_delay))

flights %>% count(
  dep_missing = is.na(dep_time), 
  arr_missing = is.na(arr_time)
)
```

You can combine `count()` with the `cut_*` functions from `ggplot2` to compute histograms
"by hand":

```
# five bins of equal widths
flights %>% count(cut_interval(arr_delay, 5))

# five bins with approximately equal numbers of points
flights %>% count(cut_number(arr_delay, 5))

# hourly bins
flights %>% count(cut_width(arr_delay, 60, boundary = 0))
```

### Readings

- [Introduction](http://r4ds.had.co.nz/exploratory-data-analysis.html#introduction-3)
  [r4ds-7.1]

- [Questions](http://r4ds.had.co.nz/exploratory-data-analysis.html#questions)
  [r4ds-7.2]

- [Variation](http://r4ds.had.co.nz/exploratory-data-analysis.html#variation)
  [r4ds-7.3]

## Tidy data `[wrangle]`

A **tidy** data frame is a data frame (or tibble) where the variables in columns, and the cases
(or observations) are in the rows. All the of the built-in datasets that you've worked with so far
have been tidy, but you'll soon encounter new datasets that need to be tidied before you can work
with them.

- [Introduction](http://r4ds.had.co.nz/tidy-data.html#introduction-6)
  [r4ds-12.1]

- [Tidy date](http://r4ds.had.co.nz/tidy-data.html#tidy-data-1)
  [r4ds-12.2]

## ggplot2 calls `[visualize]`

As you get more familiar with `ggplot2`, you can start using a more concise
expression of ggplot2 code.

- [ggplot2 calls](http://r4ds.had.co.nz/exploratory-data-analysis.html#ggplot2-calls)
  [r4ds-7.7]