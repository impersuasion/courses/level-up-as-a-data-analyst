# Baseball: Exploring Data in a Relational Database

We will explore the change in player salaries and team payrolls over time.

## Topics

- Loading CSV files into a relational database.
- Read schema for a database to understand how the database is organized
- Use basic `SELECT` statements, including `WHERE` clause, to access and
  retrieve subsets of data stored in a single table.
- Merge and combine information within and across tables with clauses such as
  `GROUP BY`, `HAVING`, and `LEFT OUTER JOIN`.
- Retrieve data from a query in blocks and further process it in R.

## Loading CSV files into a relational database

We'll be using `dplyr` to load our CSV files into an SQLite database. Once we
have the tables ready, we can use `dplyr` to work database tables just like data
frames. Let's make sure we have `tidyverse` loaded.

```{r}
library(tidyverse)
```

### Creating a new database

```{r}
# create the database
baseball_db <- src_sqlite("data/baseball.sqlite3", create = TRUE)
```

### Creating a table

Usually when designing a relational database, we have to come up with the schema for each table.
The schema design should match our application requirements. Fortunately, we don't have to go
through that exercise because we have our data in separate CSV files, so we can just load them
directly into database tables and work with them as if they were our usual data frames.

Let's read a CSV file into a data frame.

```{r}
Batting <- read_csv("data/core/Batting.csv")
```

Did it work? You may want to go through the
[Data Import chapter in R4DS](https://r4ds.had.co.nz/data-import.html) to figure out how to get past
this issue.

We can now create a table in our database using this data frame.

```{r}
copy_to(baseball_db, Batting, name = "Batting",
  indexes = list("playerID", "yearID", "teamID"), temporary = FALSE)
```

What do you think the `indexes` parameter is used for?

Once the data is in a database table, we can work with it directly instead of loading data from
disk. This means that the heavy lifting is done inside the database system which is much better at
working with large datasets compared to R.

```{r}
batting_tbl <- tbl(baseball_db, "Batting")
```

`batting_tbl` is a reference to the `Batting` table in our database. We can work with it as if it
were a local data frame loaded from a file:

```{r}
batting_tbl %>% count()
```

Behind the scenes, `dplyr` is translating your R code into SQL. You can see the SQL it’s generating
with `show_query()`:

```{r}
batting_tbl %>% count() %>% show_query()
```

We can even run arbitrary SQL queries on our database:

```{r}
baseball_db %>% tbl(sql("select count(*) from Batting"))
```

Typically, you’ll iterate a few times before you figure out what data you need from the database.
Once you’ve figured it out, use collect() to pull all the data down into a local tibble:

```{r}
batting_tbl %>% count() %>% collect()
```

### Your turn

Let's also create database tables for the following CSV files:

- `Salaries.csv`
- `SeriesPost.csv`
- `Teams.csv`

## Exploratory Data Analysis

### Aggregating Salaries into Payroll

To calculate the team payrolls, we need to compute the sum of the salaries for the players on each
team for each year. 

To determine how large the table is, we can calculate the number of rows in the table with:

```{r}
baseball_db %>% tbl(sql("select count(*) from Salaries"))
```

Since the table is not very large, we can retrieve the entire table:

```{r}
baseball_db %>% tbl(sql("select * from Salaries"))
```

Or we can retrieve only the variables necessary to compute the annual team payrolls:

```{r}
baseball_db %>% tbl(sql("select salary, teamID, yearID from Salaries"))
```

Let’s explore `Salaries` a little more before we compute the team payrolls. For which years do we
have salary information? Most versions of SQL contain `MIN()` and `MAX()` functions, and we can use
them to address this question:

```{r}
baseball_db %>% tbl(sql("select min(yearID), max(yearID) from Salaries"))
```

To determine whether we have salary data for all years between 1985 and 2012, we can extract the
unique values for year from `Salaries` using `DISTINCT`:

```{r}
baseball_db %>% tbl(sql("select distinct yearID from Salaries"))
```

We can also ask whether or not the other tables in the database cover the same time period. Can you
check the `Teams` table to see if it has earlier records?

## Challenges

Try to solve these using both `dplyr` and SQL queries. You may need to create some more tables.

### World Series

1. Which team lost the World Series each year? Do these teams also have high payrolls? Some argue
   that teams with lower payrolls make it into the post season playoffs, but typically don’t win
   the World Series. Do you find any evidence of this?

2. Do you see a relationship between the number of games won in a season and winning the World
   Series?
   
### Team Payroll

3. Augment the team payrolls to include each team’s name, division, and league. Create a
   visualization that includes this additional information.
   
4. One might expect a team with old players to be paying these veteran players high salaries near
   the end of their careers. Teams with a large number of mature players would therefore have a
   large payroll. Is there any evidence supporting this?
   
5. Examine the distribution of salaries of individual players over time for different teams.
   
### Players

6. Not all of the people in the database are players, e.g., some are managers. How many are
   players? How many are managers? How many are both, or neither?

7. What are the top 10 collegiate producers of major league baseball players? How many colleges are
   represented in the database? Be careful in handling those records for players who did not attend
   college.

8. Has the distribution of home runs for players increased over the years?

9. Look at the distribution of how well batters do. Does this vary over the years? Do the same
   players excel each year? Is there a clustering, a bimodal distribution?

10. Are Hall-of-Fame players, in general, inducted because of rare, outstanding performances, or
    are they rewarded for consistency over years?

11. Do pitchers get better with age? Is there an improvement and then a fall off in performance? Is
    this related to how old they are, or the number of years they have been pitching? What about
    the league they are in? Do we have information about each of these factors? If so, how can we
    combine them to present information about the general question?

### Miscellaneous

12. How complete are the records for the earliest seasons recorded in this database? For example,
    we know that there is no salary information prior to 1985, but are all of the other tables
    “complete”?

13. Are certain baseball parks better for hitting home runs? Can we tell from this data? Can we
    make inferences about this question?

14. What is the distribution of the number of shut-outs for a team in a season?