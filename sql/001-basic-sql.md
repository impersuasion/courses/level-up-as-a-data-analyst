# SQL for Data Analysis


## What is SQL?

Structured Query Language (SQL) is a programming language for working with data stored in a
*relational database*.

SQL is used for accessing, cleaning, and analyzing data that’s stored in databases. It’s easy to
learn, yet it can be used to answer complex questions using data.


## What's a database?

From [Wikipedia](https://en.wikipedia.org/wiki/Database): A database is an organized collection
of data.

Why do we need databases at all? Can't we just use flat files (i.e. comma-separated values?), or
an MS Excel spreadsheet?

Here's a list of things that you must use databases for.

- Managing complex data
- Keeping data consistent
- Performance
- Simultaneous access
- SQL support


## SQL SELECT

### Basic syntax: `SELECT` and `FROM`

```sql
SELECT year,
       month,
       west
  FROM us_housing_units;
```

- The query is telling the database to return the `year`, `month`, and `west` columns from the
  table `us_housing_units`.
- Note that the three column names are separated by a comma in the query.

If you want to select all columns in the table, you can use `*` instead of the column names:

```sql
SELECT *
  FROM us_housing_units;
```

#### Practice

Write a query to select all of teh columns in the `us_housing_units` table without using
`*`.


## SQL LIMIT

`LIMIT` restricts how many rowns the SQL query returns.

```sql
SELECT *
  FROM us_housing_units
LIMIT 100;
```

#### Practice

Write a query that uses the `LIMIT` command to restrict the result set to only 15 rows.


## SQL WHERE

Once you know how to view some data using `SELECT` and `FROM`, the next step is filtering the data
using the `WHERE` clause:

```sql
SELECT *
  FROM us_housing_units
 WHERE month = 1;
```


## SQL Comparison Operators

The most basic way to filter data is using comparison operators.

Operator                 | Syntax
---                      | ---
Equal to                 | `=`
Not equal to             | `<>` or `!=`
Greater than             | `>`
Less than                | `<`
Greater than or equal to | `>=`
Less than or equal to    | `<=`

### Comparison operators on numerical data

```sql
SELECT *
  FROM us_housing_units
 WHERE west > 30;
```

#### Practice

Did the West Region ever produce *more than 50,000* housing units in one month?

#### Practice

Did the South Region ever produce *20,000 or fewer* housing units in one month?

### Comparison operators on non-numerical data

```sql
SELECT *
  FROM us_housing_units
 WHERE month_name != 'January';
```

If you are using an operator with values that are non-numeric, you need to put the value in single
quotes: `'value'`.

**Note:** SQL uses single quotes to reference column values.

You can use `<`, `>`, and the rest of comparison operators on non-numeric columns as well - they
filter based on *lexicographical order*.

```sql
SELECT *
  FROM us_housing_units
 WHERE month_name > 'J';
```

Why is `January` included in the results?

#### Practice

Write a query that only shows rows for which the month name is February.

#### Practice

Write a query that only shows rows for which the `month_name` starts with the letter `N` or an
earlier letter in the alphabet.

### Arithmetic in SQL

You can perform arithmetic in SQL using the same operators you would in Excel: `+`, `-`, `*`, and
`/`.

- You can only perform arithmetic across columns on values in a given row
- What if you want to add values across multiple rows?

```sql
SELECT year,
       month,
       west,
       south,
       west + south AS south_plus_west
  FROM us_housing_units;
```

The columns that contain the arithmetic functions are called "derived columns" because they are
generated from the underlying data.

You can use parentheses to manage the order of operations. It is a good idea to use parentheses
even when they are not required just to make your query easier to read.

#### Practice

Write a query that calculates the sum of all four regions in a separate column.

#### Practice

Write a query that returns all rows for which more units were produced in the West region than in
the Midwest and Northeast combined.

#### Practice

Write a query that calculates the percentage of all houses completed in the United States
represented by each region. Only return results from the year 2000 and later.

**Hint:** There should be four columns of percentages.


## SQL Logical Operators

Logical operators allow you to use multiple comparison operators in one query:

- `LIKE` allows you to match similar values, instead of exact values.
- `IN` allows you to specify a list of values you'd like to include.
- `BETWEEN` allows you to select only rows within a certain range.
- `IS NULL` allows you to select rows that contain now data in a given column.
- `AND` allows you to select only rows that satisfy two conditions.
- `OR` allows you to select rows that satisfy either of two conditions.
- `NOT` allows you to select rows that do not match a certain condition.

We will be using data from [Billboard Music Charts](https://www.billboard.com/charts). It was
collected in January 2014 and contains data from 1956 through 2013. The results in this table are
the *year-end* results - the top 100 songs at the end of each year.

```sql
SELECT *
  FROM billboard_top_100_year_end;
```

- `year_rank` is the rank of tha song at the end of the listed year.
- `group` is the name of the entire group that won (this could be multiple artists).
- `artist` is an individual artist. This is a little complicated, as an artist can be an
   individual or group.

```sql
SELECT *
  FROM billboard_top_100_year_end
 ORDER BY year DESC, year_rank;
```


## SQL LIKE

`LIKE` is a logical operator in SQL that allows you to match on similar values rather than exact
ones.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE "group" LIKE 'Snoop%'
```

**Note:** `"group"` appears in quotations above because `GROUP` is actually the name of a function
in SQL. The double quotes (as opposed to single: `'`) are a way of indicating that you are
referring to the column name `"group"`, not the SQL function.

### Wildcards and ILIKE

The `%` used above represents any character or set of characters. In this case, `%` is referred to
as a "wildcard." If `LIKE` is case-sensitive in the type of SQL you are using, you can ignore case
using the `ILIKE` command:

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE "group" ILIKE 'snoop%';
```

You can also use `_` (a single underscore) to substitute for an individual character:

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE artist ILIKE 'dr_ke';
```

#### Practice

Write a query that returns all rows for which Ludacris was a member of the group.

#### Practice

Write a query that returns all rows for which the first artist listed in the group has a name that
begins with "DJ".


## SQL IN

`IN` is a logical operator in SQL that allows you to specify a list of valus that you'd like to
include in the results.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank IN (1, 2, 3);
```

You can also use non-numerical values.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank IN ('Taylor Swift', 'Usher', 'Ludacris');
```

#### Practice

Write a query that shows all the entries for Elvis and M.C. Hammer.

**Hint:** M.C. Hammer is actually on the list under multiple names, so you may need to first
write a query to figure out exactly how M.C. Hammer is listed. You're likely to face similar
problems that require some exploration in many real-life scenarios.


## SQL BETWEEN

`BETWEEN` is a logical operator in SQL that allows you to select only rows that are within a
specific range. It has to be paired with the `AND  `operator.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank BETWEEN 5 AND 10;
```

`BETWEEN` includes the range bounds (in this case, 5 and 10) that you specify in the query, in
addition to the values between them. So the above query is equivalent to:

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank >= 5 AND year_rank <= 10;
```

Some people prefer this version because it explicitly shows what the query is doing.

#### Practice

Write a query that shows all top 100 songs from January 1, 1985 through December 31, 1990


## SQL IS NULL

`IS NULL` is a logical operator in SQL that allows you to exclude rows with missing data from your
results.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE artist IS NULL;
```

`WHERE artist = NULL` will **not** work - you can't perform arithmetic on null values.

#### Practice

Write a query that shows all of the rows for which `song_name` is null.


## SQL AND

`AND` is a logical operator in SQL that allows you to select only rows that satisfy two
conditions.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2012 AND year_rank <= 10;
```

You can combine multiple comparison operators using `AND`.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2012
   AND year_rank <= 10
   AND "group" ILIKE '%feat%';
```

#### Practice

Write a query that surfaces all rows for top-10 hits for which Ludacris is part of the group.

#### Practice

Write a query that surfaces the top-ranked records in 1990, 2000, and 2010.

#### Practice

Write a query that lists all songs from the 1960s with "love" in the title.


## SQL OR

`OR` is a logical operator in SQL that allows you to select rows that satisfy either of two
conditions. It works the same way as `AND`.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank = 5 OR artist = 'Gotye';
```

You can combine `AND` with `OR` using parentheses.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2013
   AND ("group" ILIKE '%macklemore%' OR "group" ILIKE '%timberlake%');
```

#### Practice

Write a query that results all rows for top-10 songs that featured either Kay Perry or Bon Jovi.

#### Practice

Write a query that returns all songs with titles that contain the word "California" in either the
1970s or 1990s.

#### Practice

Write a query that lists all top-100 recordings that feature Dr. Dre before 2001 or after 2009.


## SQL NOT

`NOT` is a logical operator in SQL that you can put before any conditional statement to select
rows for which that statement is false.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE yaer = 2013
   AND year_rank NOT BETWEEN 2 AND 3;
```

`NOT` is commonly used with `LIKE`.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2013
   AND "group" NOT ILIKE '%macklemore%';
```

`NOT` is also frequently used to identify non-null rows, but the syntax is somewhat special - you
need to include `IS` beforehand.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2013
   AND artist IS NOT NULL;
```

#### Practice

Write a query that returns all rows for songs that were on the charts in 2013 and do not contain
the letter "a".


## SQL ORDER BY

### Sorting data with SQL ORDERY BY

The `ORDER BY` clause allows you to sort your results based on the data in one or more columns.

```sql
SELECT *
  FROM billboard_top_100_year_end
 ORDER BY artist;
```

Notice that the results are now ordered alphabetically from a to z based on the content in the
`artist` column.

By default, SQL sorts the data in ascending order. If you'd like your results in descending order,
you need to add the `DESC` operator.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year = 2013
 ORDER BY year_rank DESC;
```

#### Practice

Write a query that returns all rows from 2012, ordered by song title from Z to A.

### Ordering data by multiple columns

You can also order by multiple columns.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank <= 3
 ORDER BY year DESC, year_rank;
```

You will sometimes see `ORDER BY` clause used with column numbers instead of column names. The
numbers will correspond to the order in which you list columns in the `SELECT` clause.

```sql
SELECT *
  FROM billboard_top_100_year_end
 WHERE year_rank <= 3
 ORDER BY 1 DESC, 2;
```

**Note:** This functionality (numbering columns instead of using names) is not supported by every
flavor of SQL.

**Note:** When using `ORDER BY` with a row limit, the ordering clause is executed first.

### Using comments

It can be helpful to include comments that explain your thinking. Comments are not treated like
SQL code, so they can also be used to disable pieces of code.

```sql
SELECT *  -- this is random comment that has no effect on the code
  FROM billboard_top_100_year_end
 WHERE year = 2012
   -- AND year_rank <= 10
   -- the above condition is disabed because we commented it out
   AND "group" ILIKE '%feat%';
```

#### Practice

Write a query that shows all rows for which T-Pain was a group member, ordered by rank on the
charts, from lowest to highest rank (from 100 to 1).

#### Practice

Write a query that returns songs that ranked between 10 and 20 (inclusive) in 1993, 2003, or 2013.
Order the results by year and rank, and leave a comment on each line of the `WHERE` clause to
indicate what that line does.
